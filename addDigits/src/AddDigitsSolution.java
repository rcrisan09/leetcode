/**
 * Created by thewo on 06.01.2017.
 */
public class AddDigitsSolution {
    public static void main(String[] args) {
        System.out.println(addDigits(38));
    }

    static public int addDigits(int num) {
        int s = 0;
        while (num > 9) {
            while (num != 0) {
                s += num % 10;
                num /= 10;
            }
            num = s;
            s = 0;
        }
        return num;
    }
}
