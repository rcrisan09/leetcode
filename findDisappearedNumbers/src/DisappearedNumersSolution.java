import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thewo on 28.12.2016.
 */
public class DisappearedNumersSolution {
    public static void main(String[] args) {
        System.out.println(findDisappearedNumbers(new int[]{}));
    }

    public static List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> result = new ArrayList<>();
        Arrays.sort(nums);
        for (int i = 1; i <= nums.length; i++) {
            if (Arrays.binarySearch(nums, i) < 0) {
                result.add(i);
            }
        }
        return result;
    }
}
