import java.util.Arrays;

/**
 * Created by thewo on 29.12.2016.
 */
public class FindTheDifferenceSolution {
    public static void main(String[] args) {
        System.out.println(findTheDifference("", "a"));
    }

    public static char findTheDifference(String s, String t) {
        long sum1 = 0;
        long sum2 = 0;
        for (int i = 0; i < s.length(); i++) {
            sum1 += s.charAt(i);
            sum2 += t.charAt(i);
        }
        sum2 += t.charAt(t.length() - 1);
        return (char) (sum2 - sum1);
    }
}
