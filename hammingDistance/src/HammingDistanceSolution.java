import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thewo on 27.12.2016.
 */
public class HammingDistanceSolution {
    public static void main(String[] args) {
        System.out.println(hammingDistance(24, 28));
    }

    public static int hammingDistance(int x, int y) {
        String xBytes = Integer.toBinaryString(x);
        String yBytes = Integer.toBinaryString(y);

        int distance = 0;
        int xLen = xBytes.length(), yLen = yBytes.length();

        StringBuilder zeroes = new StringBuilder();
        int k = Math.abs(xLen - yLen);
        for (int i = 0; i < k; i++) {
            zeroes.append("0");
        }

        if (xLen < yLen) xBytes = zeroes.toString().concat(xBytes);
        else yBytes = zeroes.toString().concat(yBytes);

        for (int i = 0, count = 0; i < xBytes.length(); i++) {
            while (i < xBytes.length() && xBytes.charAt(i) != yBytes.charAt(i)) {
                count++;
                i++;
            }
            distance = Math.max(distance, count);
        }
        System.out.println(xBytes);
        System.out.println(yBytes);
        return distance;
    }
}
