import java.util.*;

/**
 * Created by thewo on 03.12.2016.
 */
public class IntegerToRomanSolution {
    public static void main(String[] args) {
        System.out.println(intToRoman(1523));
    }

    public static String intToRoman(int num) {
        HashMap<Integer, Character> romanCode = new HashMap<>();
        romanCode.put(1000, 'M');
        romanCode.put(500, 'D');
        romanCode.put(100, 'C');
        romanCode.put(50, 'L');
        romanCode.put(10, 'X');
        romanCode.put(5, 'V');
        romanCode.put(1, 'I');

        List<Character> result = new ArrayList<>();
        int p = 1;

        while (num != 0) {
            int digit = num % 10;
            if (romanCode.containsKey(digit * p)) {
                result.add(romanCode.get(digit * p));
            } else if (digit <= 3) {
                appendAsManyTimes(digit, romanCode.get(p), result);
            } else if (digit % 5 != 4) {
                appendAsManyTimes(digit % 5, romanCode.get(p), result);
                result.add(romanCode.get((digit - digit % 5) * p));
            } else {
                result.add(romanCode.get((digit + 1) * p));
                result.add(romanCode.get(p));
            }
            num /= 10;
            p *= 10;
        }
        Collections.reverse(result);
        return listToString(result);
    }

    private static String listToString(List<Character> list) {
        StringBuilder result = new StringBuilder();
        list.forEach(result::append);
        return result.toString();
    }

    private static void appendAsManyTimes(int times, char letter, List<Character> result) {
        while (times > 0) {
            result.add(letter);
            times--;
        }
    }
}
