/**
 * Created by thewo on 28.12.2016.
 */
public class IslandParameterSolution {
    public static void main(String[] args) {
        System.out.println(islandPerimeter(new int[][]{{0, 1, 0, 0},
                {1, 1, 1, 0},
                {0, 1, 0, 0},
                {1, 1, 0, 0}}));
    }

    public static int islandPerimeter(int[][] grid) {
        int parameter = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] > 0) {
                    parameter += marginsAround(i, j, grid) + edgesSurroundedByWater(i, j, grid);
                }
            }
        }
        return parameter;
    }

    private static int edgesSurroundedByWater(int i, int j, int[][] grid) {
        int water = 0;
        if (j != 0 && grid[i][j - 1] == 0) water++;
        if (j != grid[i].length - 1 && grid[i][j + 1] == 0) water++;
        if (i != 0 && grid[i - 1][j] == 0) water++;
        if (i != grid.length - 1 && grid[i + 1][j] == 0) water++;
        return water;
    }

    private static int marginsAround(int i, int j, int[][] grid) {
        int margins = 0;
        if (i == 0) margins++;
        if (j == 0) margins++;
        if (i == grid.length - 1) margins++;
        if (j == grid[i].length - 1) margins++;
        return margins;
    }
}
