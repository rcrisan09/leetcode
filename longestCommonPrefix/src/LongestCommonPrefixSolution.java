/**
 * Created by thewo on 04.12.2016.
 */
public class LongestCommonPrefixSolution {
    public static void main(String[] args) {
        String[] array = {"abc", "ab", "abzzaa", "abcd", "abcde", "abcd"};

        System.out.println(longestCommonPrefix(array));
        array = new String[]{"c","acc","ccc"};
        System.out.println(longestCommonPrefix(array));
    }

    public static String longestCommonPrefix(String[] strs) {
        if (strs.length == 0 || strs[0].equals("")) return "";
        if (strs.length == 1) return strs[0];

        StringBuilder longestPrefix = new StringBuilder("");
        for (int i = 0; i < strs[0].length(); i++) {
            if (checkForPrefix(longestPrefix.toString() + strs[0].charAt(i), strs)) {
                longestPrefix.append(strs[0].charAt(i));
            } else break;
        }
        return longestPrefix.toString();
    }

    private static boolean checkForPrefix(String prefix, String[] strs) {
        for (String s : strs) {
            if (!s.startsWith(prefix)) return false;
        }
        return true;
    }

}
