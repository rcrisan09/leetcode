import java.util.*;

/**
 * Created by thewo on 21.12.2016.
 */
public class LongestSubstringSolution {
    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("dvdf"));
    }

    public static int lengthOfLongestSubstring(String s) {
        if (s == null || s.isEmpty()) {
            return 0;
        }
        int start = 0, max = 0, i = 0;
        Set<Character> characters = new HashSet<>();
        while (i < s.length()) {
            if (!characters.contains(s.charAt(i))) {
                characters.add(s.charAt(i++));
                max = Math.max(max, characters.size());
            } else {
                characters.remove(s.charAt(start++));
            }
        }
        return max;
    }
}
