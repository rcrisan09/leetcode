/**
 * Created by thewo on 03.12.2016.
 */
public class PalindromeNumberSolution {

    public static void main(String[] args) {
        System.out.println(isPalindrome(-141));
    }

    public static boolean isPalindrome(int x) {
        return x == revert(x);
    }

    private static int revert(int x) {
        int numberReversed = 0;
        while (x != 0) {
            numberReversed = numberReversed * 10 + x % 10;
            x /= 10;
        }
        return numberReversed;
    }
}
