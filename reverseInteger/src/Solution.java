/**
 * Created by thewo on 30.11.2016.
 */
public class Solution {
    public int reverse(int x) {
        boolean negative = false;
        if (x < 0) {
            negative = true;
            x *= -1;
        }
        long r = 0;
        while (x != 0) {
            r = r * 10 + x % 10;
            x /= 10;
            if (r > Integer.MAX_VALUE || r < Integer.MIN_VALUE) return 0;
        }
        return (int) (negative ? -r : r);
    }
}
