import java.util.HashMap;

/**
 * Created by thewo on 30.11.2016.
 */
public class SolutionRomanToInteger {

    public int romanToInt(String s) {
        HashMap<Character, Integer> romanCode = new HashMap<>();
        romanCode.put('M', 1000);
        romanCode.put('D', 500);
        romanCode.put('C', 100);
        romanCode.put('L', 50);
        romanCode.put('X', 10);
        romanCode.put('V', 5);
        romanCode.put('I', 1);

        int r = 0;
        r += romanCode.get(s.charAt(0));
        for (int i = 1; i < s.length(); i++) {
            int previousValue = romanCode.get(s.charAt(i - 1));
            int currentValue = romanCode.get(s.charAt(i));

            if (currentValue <= previousValue) {
                r += currentValue;
            } else {
                currentValue -= previousValue;
                r -= previousValue;
                r += currentValue;
            }
        }
        return r;
    }

}
