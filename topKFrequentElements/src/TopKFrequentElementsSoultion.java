import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by thewo on 11.02.2017.
 */
public class TopKFrequentElementsSoultion {

    public static void main(String[] args) {
        System.out.println(topKFrequent(new int[]{1, 1, 1, 2, 2, 3}, 2));
    }

    public static List<Integer> topKFrequent(int[] nums, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                map.merge(nums[i], 1, Integer::sum);
            } else {
                map.put(nums[i], 1);
            }
        }
        Map<Integer, Integer> result = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1, LinkedHashMap::new));
        return result.keySet().stream().limit(k).collect(Collectors.toList());
    }


}
